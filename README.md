# Socket Market

## Requisitos
- Listar os produtos disponível
- Colocar um produto no carrinho
- Remover um produto do carrinho
- Pagar o pedido
- Solicitar a entrega

## Alunos
- Felipe dos Santos Goiabeira
- Claudio Henrique Velozo Alexandre

## Rodar o projeto
### Server

```
node /server/server.js
```
### Cliente

```
node /client/client.js
```

